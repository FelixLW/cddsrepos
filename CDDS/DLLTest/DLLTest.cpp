#include "pch.h"
#include "CppUnitTest.h"
#include "../Double-Linked Lists/DoubleLinkedList.h"
#include <iostream>
#include <string>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace DLLTest
{
	TEST_CLASS(DLLTest)
	{
	public:
		
		TEST_METHOD(DoubleLinkedListTest)
		{
			CDDS::DoubleLinkedList<std::string> l;

			Assert::IsTrue(l.empty());
			Assert::IsTrue(l.count() == 0);

			try
			{
				std::cout << l.first();
				Assert::Fail(L"Exception not thrown");
			}
			catch (CDDS::DoubleLinkedList<std::string>::ListIsEmptyException e)
			{

			}

			try
			{
				std::cout << l.last();
				Assert::Fail(L"Exception not thrown");
			}
			catch (CDDS::DoubleLinkedList<std::string>::ListIsEmptyException e)
			{

			}

			l.pushFront("second");
			Assert::IsTrue(l.count() == 1);
			Assert::IsFalse(l.empty());
			Assert::IsTrue(l.first() == "second");
			Assert::IsTrue(l.last() == "second");

			l.pushFront("first");
			Assert::IsTrue(l.count() == 2);
			Assert::IsFalse(l.empty());
			Assert::IsTrue(l.first() == "first");
			Assert::IsTrue(l.last() == "second");

			l.pushBack("third");
			Assert::IsTrue(l.count() == 3);
			Assert::IsTrue(l.first() == "first");
			Assert::IsTrue(l.last() == "third");



			CDDS::DoubleLinkedList<std::string>::Iterator iter = l.begin();
			Assert::IsTrue(iter.isValid());
			Assert::IsTrue(iter.GetCurrent() == "first");

			iter = l.end();
			Assert::IsFalse(iter.isValid());

			iter = l.begin();
			Assert::IsTrue(iter.isValid());
			Assert::IsTrue(iter.GetCurrent() == "first");

			iter.MoveNext();
			Assert::IsTrue(iter.isValid());
			Assert::IsTrue(iter.GetCurrent() == "second");

			iter++;
			Assert::IsTrue(iter.isValid());
			Assert::IsTrue(iter.GetCurrent() == "third");

			iter.MoveNext();
			Assert::IsFalse(iter.isValid());

			Assert::IsTrue(l.first() == "first");
			l.popFront();
			Assert::IsTrue(l.first() == "second");

			l.pushFront("zero");
			l.popBack();
			Assert::IsTrue(l.count() == 2);
			Assert::IsTrue(l.last() == "second");

			l.popFront();
			l.popBack();

			try
			{
				l.popFront();
				Assert::Fail(L"Exception not thrown");
			}
			catch (CDDS::DoubleLinkedList<std::string>::ListIsEmptyException e)
			{

			}

			Assert::IsTrue(l.empty());

			l.pushFront("789");
			l.pushFront("456");
			l.pushFront("123");

			CDDS::DoubleLinkedList<std::string>::Iterator iterToErase = l.begin();


			iterToErase.MoveNext();
			l.erase(iterToErase);
			Assert::IsTrue(l.count() == 2);

			iterToErase = l.begin();

			iterToErase.MoveNext();
			l.erase(iterToErase);
			Assert::IsTrue(l.count() == 1);
			Assert::IsTrue(l.first() == "123");
			Assert::IsTrue(l.last() == "123");

			l.clear();
			Assert::IsTrue(l.empty());
			Assert::IsTrue(l.count() == 0);

			l.pushFront("123");
			l.pushFront("123");
			l.pushFront("124");
			l.pushFront("123");

			l.remove("123");
			Assert::IsTrue(l.count() == 1);

			l.clear();
			Assert::IsTrue(l.empty());



			l.pushFront("abc");
			l.pushFront("def");
			l.pushFront("ghi");

			CDDS::DoubleLinkedList<std::string>::Iterator iterToIns = l.begin();

			iterToIns.MoveNext();

			l.insert(iterToIns, "jkl");
			Assert::IsTrue(l.count() == 4);

			l.clear();
			Assert::IsTrue(l.empty());

			l.pushFront("4");
			l.pushFront("2");
			l.pushFront("3");
			l.pushFront("1");
			l.pushFront("5");

			CDDS::DoubleLinkedList<std::string>::Iterator iterToSort = l.begin();

			l.sort();

			Assert::IsTrue(l.first() == "1");
			Assert::IsTrue(l.last() == "5");			
		}
	};
}
