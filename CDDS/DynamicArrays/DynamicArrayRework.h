#pragma once
#include <algorithm>

const unsigned int defAllocated = 10;

enum DynamicArrayException
{
	ARRAY_IS_EMPTY,
	OUT_OF_BOUNDS
};

template <typename T>
class DynamicArray
{
public:

	DynamicArray(unsigned int initialAllocated = defAllocated)
	{
		data = nullptr;
		allocateArrayData(initialAllocated);
	}
	DynamicArray(DynamicArray& other)
	{
		data = nullptr;
		operator=(other);
	}
	~DynamicArray()
	{
		freeArrayData();
	}

	DynamicArray& operator=(DynamicArray& other)
	{
		allocateArrayData(other.allocatedElements);
		copyArrayData(other);
		usedElements = other.usedElements;

		return *this;
	}

	T& operator[](unsigned int index) const
	{
		if (index >= allocatedElements)
		{
			throw OUT_OF_BOUNDS;
		}

		return data[index];
	}

	unsigned int PushBack(const T& item)
	{
		if (usedElements >= allocatedElements)
		{
			Resize(Capacity() * 2);
		}

		data[usedElements] = item;
		usedElements++;

		return Size();
	}

	T& PopBack()
	{
		if (usedElements <= 0)
		{
			throw ARRAY_IS_EMPTY;
		}
		usedElements--;
		return data[usedElements];
	}

	unsigned int Insert(unsigned int index, const T& item)
	{
		if (index >= usedElements)
		{
			return PushBack(item);
		}

		if (usedElements >= allocatedElements)
		{
			Resize(Capacity() * 2);
		}

		for (unsigned int i = usedElements - 1; i >= index; i--)
		{
			data[i + 1] = data[i];
		}

		data[index] = item;

		usedElements++;

		return Size();
	}

	void Erase(unsigned int index, bool ordered = false)
	{
		if (index >= usedElements)
		{
			throw OUT_OF_BOUNDS;
		}

		if (ordered)
		{
			for (unsigned int i = index; i < usedElements - 1; i++)
			{
				data[i] = data[i + 1];
			}
		}

		else
		{
			unsigned int lastElementIndex = usedElements - 1;
			if (lastElementIndex > index)
			{
				data[index] = data[lastElementIndex];
			}
		}

		usedElements--;
	}

	void Clear()
	{
		usedElements = 0;
	}

	void Resize(unsigned int newAllocatedElements)
	{
		//Set minimum of size of 1
		if (newAllocatedElements < 1)
			newAllocatedElements = 1;

		T* temp = new T[newAllocatedElements];

		if (data != nullptr)
		{
			unsigned int minAllocatedElements = std::min(allocatedElements, newAllocatedElements);
			for (unsigned int i = 0; i < minAllocatedElements; i++)
			{
				temp[i] = data[i];
			}

			//Release old memory
			delete[] data;
		}

		data = temp;
		allocatedElements = newAllocatedElements;
	}

	unsigned int Size()
	{
		return usedElements;
	}

	unsigned int Capacity()
	{
		return allocatedElements;
	}
	

protected:

	void allocateArrayData(unsigned int newAllocated)
	{
		freeArrayData();

		allocatedElements = newAllocated;
		data = new T[allocatedElements];

		usedElements = 0;
	}

	void copyArrayData(DynamicArray& other)
	{
		unsigned int minAllocated = std::min(allocatedElements, other.allocatedElements);
		for (unsigned int i = 0; i < minAllocated; i++)
		{
			data[i] = other.data[i];
		}
	}

	void freeArrayData()
	{
		if (data != nullptr)
		{
			delete[] data;
			data = nullptr;
		}
	}

	T* data;
	unsigned int usedElements;
	unsigned int allocatedElements;
};