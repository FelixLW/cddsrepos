#pragma once
#pragma warning(disable:4244)

#include <iostream>

namespace CDDS
{
    template <class T>
    class DynamicArray
    {
    public:
        DynamicArray()
        {
            // Allocates a single memory block
            maxsize = 1;
            list = new T[maxsize];
            length = 0;
        }

        ~DynamicArray()
        {
            delete[] list;
        }

        // Insert a new item to the end of the list
        void add(T item)
        {
            if (isFull())
            {
                if (length > 100)
                {
                    // Create temporary list with increased size
                    maxsize *= 1.5f;
                }
                else
                {
                    // Create temporary list with double size
                    maxsize *= 2;
                }

                T* temp_list = new T[2 * maxsize];

                // Move all the elements to the temporary list
                for (int i = 0; i < length; i++)
                {
                    temp_list[i] = list[i];
                }

                // Delete the old list
                delete[] list;

                // Rename temp list
                list = temp_list;

                std::cout << "List Increased." << std::endl << std::endl;
            }
            else
            {
                // Add the new item at the end of the list
                list[length] = item;
                length++;
            }
        }

        void insertMiddle(T item)
        {
            list[length / 2] = item;
            length++;
        }

        void printList()
        {
            for (int i = 0; i < length; i++)
            {
                std::cout << list[i] << ' ';
            }
            std::cout << std::endl;
        }

        void printSizeCapacity()
        {
            std::cout << "Size: " << Size() << std::endl;
            std::cout << "Capacity: " << Capacity();
        }


        bool isFull()
        {
            return length == maxsize;
        }

        int Size()
        {
            return length;
        }

        int Capacity()
        {
            return maxsize;
        }

    private:
        T* list;
        int maxsize;
        int length;
    };
}