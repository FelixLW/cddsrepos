#pragma once
#include "../DLLTest/pch.h"
#include <string>

namespace CDDS
{
	template <typename T>
	class DoubleLinkedList
	{
	private:
		class ListNode
		{
		public:

			ListNode(const T& value, ListNode* _next = nullptr, ListNode* _prev = nullptr)
			{
				data = value;
				next = _next;
				prev = _prev;
			}

			T data;
			ListNode* next = nullptr;
			ListNode* prev = nullptr;
		};

	public:
		class ListIsEmptyException
			: std::exception
		{
		public:
			ListIsEmptyException(const char* msg = "List Is Empty")
				: exception(msg)
			{

			}
		};

		//Iterator
		class Iterator
		{
		private:
			friend class DoubleLinkedList;
			Iterator(ListNode* node)
			{
				this->node = node;
			}

		public:
			T& getData() const
			{
				return node->data;
			}
			void MoveNext()
			{
				if (node != nullptr)
					node = node->next;
			}

			Iterator operator++(int val)
			{
				MoveNext();
				return *this;
			}

			Iterator operator+=(int val)
			{
				for(int i = 0; i < val; i++)
					MoveNext();
				return *this;
			}

			void MovePrevious()
			{ 
				if (node != nullptr)
					node = node->prev;
			}

			Iterator operator--(int val)
			{
				MovePrevious();
				return *this;
			}

			Iterator operator-=(int val)
			{
				for (int i = 0; i < val; i++)
					MovePrevious();
				return *this;
			}

			bool isValid()
			{
				return node != nullptr;
			}

			T& GetCurrent()
			{
				//check that node !- nullptr, throw exception
				if (node == nullptr)
					throw std::exception("Invalid iterator");

				return node->data;
			}

			T& operator*()
			{
				return GetCurrent();
			}

		private:
			ListNode* node = nullptr;
		};

	private:
		ListNode* head = nullptr;
		ListNode* tail = nullptr;

	public:

		DoubleLinkedList();

		~DoubleLinkedList();

		//check is list empty
		bool empty() const;

		//return number of items in list
		unsigned int count();

		//get data at head of the list
		T& first() const;

		//get data at tail of the list
		T& last() const;

		//get iterator to start of list
		Iterator begin() 
		{
			return Iterator(head);
		}

		//get iterator to end of list
		Iterator end()
		{
			return Iterator(nullptr);
		}

		//add data to front of list
		void pushFront(const T& value);

		//add data to front of list
		void pushBack(const T& value);

		//remove node from the front
		void popFront();

		//remove node from the end
		void popBack();

		//remove from a middle node
		void erase(Iterator iter);

		//insert value at specifed location
		void insert(Iterator &i, const T& value);

		//remove nodes with chosen value
		void remove(const T& value);

		//print list
		void print(DoubleLinkedList& l, Iterator& i);

		//Free all memory in the list and reset
		void clear();

		//Bubble sort list
		void sort();

		bool swap(typename DoubleLinkedList<T>::Iterator& firstIter, typename DoubleLinkedList<T>::Iterator& secondIter);
	};

	template <typename T>
	DoubleLinkedList<T>::DoubleLinkedList()
	{
		
	}

	template <typename T>
	DoubleLinkedList<T>::~DoubleLinkedList()
	{
		clear();
	}

	template <typename T>
	bool DoubleLinkedList<T>::empty() const
	{
		return (head == nullptr) && (tail == nullptr);
	}

	template <typename T>
	unsigned int DoubleLinkedList<T>::count()
	{
		unsigned int counter = 0;

		ListNode* node = head;

		while (node != nullptr)
		{
			counter++;
			node = node->next;
		}

		return counter;
	}

	template <typename T>
	T& DoubleLinkedList<T>::first() const
	{
		if (empty())
		{
			throw ListIsEmptyException();

		}

		return head->data;
	}

	template <typename T>
	T& DoubleLinkedList<T>::last() const
	{
		if (empty())
		{
			throw ListIsEmptyException();
		}

		return tail->data;
	}

	template <typename T>
	void DoubleLinkedList<T>::pushFront(const T& value)
	{
		ListNode* newNode = new ListNode(value, head);

		if (head != nullptr)
		{
			head->prev = newNode;
		}
		head = newNode;

		if (tail == nullptr)
		{
			tail = newNode;
		}
	}

	template <typename T>
	void DoubleLinkedList<T>::pushBack(const T& value)
	{
		ListNode* newNode = new ListNode(value, nullptr, tail);

		if (tail != nullptr)
		{
			tail->next = newNode;
		}
		tail = newNode;

		if (head == nullptr)
		{
			head = newNode;
		}
	}

	template <typename T>
	void DoubleLinkedList<T>::popFront()
	{
		if (!empty())
		{
			//get a refrence to the listnode with data
			ListNode* n = head;

			head = head->next;

			//if we are moving the last node, update tail as well
			if (head == nullptr)
			{
				tail = nullptr;
			}
			else
			{
				head->prev = nullptr;
			}

			delete n;
		}
		else
		{
			throw ListIsEmptyException();
		}
	}

	template <typename T>
	void DoubleLinkedList<T>::popBack()
	{
		if (!empty())
		{
			//get a refrence to the listnode with data
			ListNode* n = tail;

			tail = tail->prev;

			//if we are moving the last node, update tail as well
			if (tail == nullptr)
			{
				head = nullptr;
			}
			else
			{
				tail->next = nullptr;
			}

			delete n;
		}
		else
		{
			throw ListIsEmptyException();
		}
	}

	template <typename T>
	void DoubleLinkedList<T>::erase(Iterator iter)
	{
		if (iter.isValid())
		{
			ListNode* node = iter.node;

			if (tail == node)
			{
				popBack();
			}
			else if (head == node)
			{
				popFront();
			}
			else 
			{
				node->next->prev = node->prev;
				node->prev->next = node->next;
				delete node;
			}			
		}
	}

	template <typename T>
	void DoubleLinkedList<T>::insert(Iterator& i, const T& value)
	{		
		ListNode* n = new ListNode(value);

		n->next = i.node;

		if (i.isValid())
		{
			n->prev = i.node->prev;

			if (i.node->prev != nullptr)
			{
				i.node->prev->next = n;
			}

			i.node->prev = n;

			if (i.node->prev == nullptr)
			{
				n = head;
			}
			if (i.node->next == nullptr)
			{
				n = tail;
			}
		}

		else if (empty())
		{
			pushFront(value);
		}
	}

	template<typename T>
	void DoubleLinkedList<T>::remove(const T& value)
	{
		Iterator iter = begin();

		while (iter.isValid())
		{
			if (iter.GetCurrent() == value)
			{
				Iterator itToErase = iter;
				iter.MoveNext();

				erase(itToErase);
			}
			else
			{
				iter.MoveNext();
			}
		}
	}

	template <typename T>
	void DoubleLinkedList<T>::print(DoubleLinkedList& l, Iterator& i)
	{
		if (!l.empty())
		{
			for (i = l.begin(); i.isValid(); i.MoveNext())
			{
				std::cout << i.GetCurrent() << std::endl;
			}
		}
		else
		{
			throw ListIsEmptyException();
		}
	}

	template <typename T>
	void DoubleLinkedList<T>::clear()
	{
		ListNode* node = head;
		ListNode* next_node;
		while (node != nullptr)
		{ 
			next_node = node->next;
			delete node;
			node = next_node;
		}

		head = tail = nullptr;
	}

	template<typename T>
	inline void DoubleLinkedList<T>::sort()
	{
		bool sorted = false;

		Iterator iter = begin();
		Iterator iterNext = begin();

		if (empty())
			return;

		while (!sorted)
		{
			iterNext.MoveNext();

			sorted = true;
			while (iterNext.isValid())
			{
				if (iter.getData() > iterNext.getData())
				{
					swap(iter, iterNext);
					sorted = false;
				}
				iter.MoveNext();
				iterNext.MoveNext();
			}
		}
	}

	template<typename T>
	bool DoubleLinkedList<T>::swap(typename DoubleLinkedList<T>::Iterator& firstIter, typename DoubleLinkedList<T>::Iterator& secondIter)
	{
		if (!firstIter.isValid() || !secondIter.isValid())
			return false;

		std::swap(firstIter.getData(), secondIter.getData());
		return true;
	}
}


