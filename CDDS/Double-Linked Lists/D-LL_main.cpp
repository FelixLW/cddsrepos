#include <iostream>
#include <string>
#include "DoubleLinkedList.h"
#include "../DynamicArrays/DynamicArray.h"

using namespace std;

int main()
{    
    CDDS::DoubleLinkedList<string> list1;
    CDDS::DoubleLinkedList<string>::Iterator iter1 = list1.begin();

    typedef CDDS::DoubleLinkedList<std::string>::ListIsEmptyException ListEmpty;

    string input;
    string word;

    bool b = true;

    cout << "'add', 'addback', addfront, 'insert', 'print', 'quit'" << endl << endl;

    do 
    {    
        cout << "Command > ";
        cin >> input;

        if (input == "add" || input == "addback")
        {
            cout << endl << "Enter word > ";
            cin >> word;

            list1.pushBack(word);

            if (list1.last() == word)
            {
                cout << '"' + word + '"' + " added successfully" << endl;
            }
            else
            {
                cout << "something went wrong" << endl;
            }
        }

        else if (input == "addfront")
        {
            cout << endl << "Enter word > ";
            cin >> word;

            list1.pushFront(word);

            if (list1.first() == word)
            {
                cout << '"' + word + '"' + " added successfully" << endl;
            }
            else
            {
                cout << "something went wrong" << endl;
            }
        }

        else if (input == "insert")
        {
            iter1 = list1.begin();
            list1.print(list1, iter1);
            string ans;

            cout << endl << "how many places from the start do you want to insert to? > ";
            cin >> ans;

            if (stoul(ans) < list1.count())
            {
                int i = 0;

                while (stoi(ans) != i)
                {
                    iter1.MoveNext();
                    i++;
                }

                list1.insert(iter1, word);
            }

            else 
            {
                cout << "Yo, the list aint that big!" << endl;
            }
        }

        else if (input == "print")
        {
            try
            {
                list1.print(list1, iter1);

            }
            catch (ListEmpty err)
            {
                cout << "List Is Empty" << endl;
            }
        }

        else if (input == "sort")
        {
            list1.sort();
        }

        else if (input == "quit")
        {
            b = false;
        }

        else
        {
            cout << "invalid input" << endl;
        }

        cout << endl;
    } while (b);

    system("pause");

    return 0;
}