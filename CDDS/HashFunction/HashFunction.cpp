#include "HashFunction.h"

namespace HashFunction 
{
	unsigned int HashFunction::badHash(const char* data, unsigned int length)
	{
		unsigned int hash = 0;

		for (unsigned int i = 0; i < length; i++) 
		{
			hash += data[i];
		}
		return 0;
	}

	unsigned int BKDRHash(const char* data, unsigned int length)
	{
		unsigned int seed = 1313;
		unsigned int hash = 0;
		unsigned int i = 0;

		for (i = 0; i < length; data++, i++)
		{
			hash = (hash * seed) + (*data);
		}

		return hash;
	}
}

