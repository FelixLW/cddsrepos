#pragma once
#include <functional>

namespace HashFunction 
{
	typedef std::function<unsigned int(const char*, unsigned int)> HashFunc;

	// Basic Addition Hash
	unsigned int badHash(const char* data, unsigned int length);

	// BKDR Hash
	unsigned int BKDRHash(const char* data, unsigned int length);

	static HashFunc defaultHash = badHash;
}

