#pragma once
#include "..\DynamicArrays\DynamicArrayRework.h"
#include <string>
#include <functional>


template<typename T>
class HashTable 
{
public:

	typedef std::pair<std::string, T> ArrayElement;
	typedef DynamicArray<ArrayElement> DataArray;

	HashTable(unsigned int size)
		: data(size)
	{
		
	}
	~HashTable()
	{

	}

	void Clear()
	{
		data.Clear();
	}

	bool Exists(const std::string& key)
	{
		ArrayElement& e = getElement(key);
		return (e.first == key);
	}

	T& Get(const std::string& key)
	{
		ArrayElement& e = getElement(key);

		if (e.first == key)
		{
			return e.second;
		}
		else
		{
			throw std::exception("Key doesn't exist");
		}
	}

	void Set(const std::string& key, const T& value)
	{
		ArrayElement& e = getElement(key);

		// Is empty?
		if (e.first == "")
		{
			e.first = key;
			e.second = value;
		}
		// does slot have data for key?
		else if (e.first == key)
		{
			e.second = value;
		}
		// collision
		else
		{
			std::string msg = std::string("Hash Collision. Key: ") + key + " <> " + e.first;
			throw std::exception(msg.c_str());
		}	
	}

	void Erase(const std::string& key)
	{
		ArrayElement& e = getElement(key);

		if (e.first == key)
		{
			e.first = "";
		}
		else
		{
			throw std::exception("Key not found");
		}
	}

	unsigned int Capacity()
	{
		return data.Capacity();
	}
	
	unsigned int Size()
	{
		return data.Size();
	}

	void Resize(unsigned int newAllocatedElements)
	{
		data.Resize(newAllocatedElements);
	}

	T& operator[](const std::string& key)
	{
		ArrayElement& e = getElement(key);
		return e.second;
	}

protected:
	ArrayElement& getElement(const std::string& key)
	{
		unsigned int hashIndex = Hash(key);

		hashIndex = hashIndex % Capacity();

		return data[hashIndex];
	}

protected:	
	//BasicAddition Hash
	unsigned int badHash(const char* data, unsigned int length)
	{
		unsigned int hash = 0;

		for (unsigned int i = 0; i < length; i++)
		{
			hash += data[i];
		}
		return 0;
	}

	//B.K., D.R. Hash
	unsigned int BKDRHash(const char* data, unsigned int length)
	{
		unsigned int seed = 1313;
		unsigned int hash = 0;
		unsigned int i = 0;

		for (i = 0; i < length; data++, i++)
		{
			hash = (hash * seed) + (*data);
		}

		return hash;
	}

	//RS Hash
	unsigned int RSHash(const char* data, unsigned int length)
	{
		unsigned int b = 378551;
		unsigned int a = 63689;
		unsigned int hash = 0;
		unsigned int i = 0;

		for (i = 0; i < length; ++data, ++i)
		{
			hash = hash * a + (*data);
			a = a * b;
		}

		return hash;
	}

	unsigned int Hash(const std::string& str)
	{
		return RSHash(str.c_str(), str.size());
	}

private:
	DataArray data;
};