#include <iostream>
#include <string>
#include "HashTable.h"

using namespace std;

void clearInputBuffer()
{
	std::cin.clear();
	std::cin.ignore(std::cin.rdbuf()->in_avail());
}

int main()
{
	clearInputBuffer();
	unsigned int htsize;
	bool b = true;

	std::cout << "Hash Table created." << endl << endl;
	std::cout << "Enter capacity of hash table" << endl << endl;
	do 
	{
		std::cout << "Command > ";
		cin >> htsize;

		if (cin.fail())
		{
			b = true;
			std::cout << "Invalid Input" << endl;
			clearInputBuffer();
			std::cout << endl;
		}
		else
		{
			b = false;
		}
		
	} while (b);
	b = true;

	HashTable<int> hTable(htsize);

	std::cout << endl << "HashTable Capacity " << hTable.Capacity() << endl;
	std::cout << "HashTable Size " << hTable.Size() << endl << endl;

	string input;
	std::string data;

	std::cout << "'quit', 'info', 'set', 'get'" << endl << endl;

	do
	{
		clearInputBuffer();
		std::cout << "Command > ";
		cin >> input;

		if (input == "set")
		{
			std::cout << "Data > ";
			cin >> data;

			try
			{
				hTable.Set(data, 123);
				std::cout << "set done" << endl;
				try
				{
					if (hTable.Exists(data))
					{
						std::cout << "set successful" << endl;
					}
				}
				catch (exception & err)
				{
					std::cout << err.what() << endl;
				}
			}
			catch (exception & e)
			{
				std::cout << e.what() << endl;
			}			
		}
		else if (input == "get")
		{
			try
			{
				std::cout << hTable.Get(data) << endl;

			}
			catch (std::exception & err)
			{
				std::cout << err.what() << endl;
			}
		}
		else if (input == "quit")
		{
			b = false;
		}
		else if (input == "info")
		{
			std::cout << endl << "HashTable Capacity " << hTable.Capacity() << endl;
			std::cout << "HashTable Size " << hTable.Size() << endl << endl;
		}
		else
		{
			std::cout << "invalid input" << endl;
		}

		std::cout << endl;

	} while (b);

	std::cout << endl;
	std::system("pause");

	return 0;
}
